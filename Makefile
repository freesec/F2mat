#   F2mat -- C lib. to manipulate matrices defined over the field F2.
#   Author: freesec at linuxmail.org
#   Copyright (C) 2019 freesec (pseudonym)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#

SHELL=/bin/bash
SD=src
LDFLAGS+=
EXTRA_INCS=nix_cprng
CFLAGS+=-Wall -I. -I$(SD) -fPIC $(foreach INC,$(EXTRA_INCS), $(addprefix -I,$(INC)/src))
CC=gcc
SRC=$(wildcard $(SD)/*.c)
OBJ=$(SRC:.c=.o)

BNAME=F2mat
SLIB=$(BNAME).a
DLIB=lib$(BNAME).so
TSRC=$(wildcard $(SD)/test*.c)
TOBJ=$(TSRC:.c=.o)
TCFLAGS=$(CFLAGS)
TLDFLAGS=$(LDFLAGS) -lcunit
TEST=test_F2_mat

all: $(SLIB) $(DLIB)

test: $(TEST)

$(SLIB): $(OBJ)
	ar cr $@ $^

$(DLIB): $(OBJ)
	$(CC) $(LDFLAGS) $(CFLAGS) -shared -o $@ $^

$(TEST):  $(TOBJ) $(SLIB) $(foreach INC,$(EXTRA_INCS), $(addsuffix .a,$(INC)/$(notdir $(INC))))
	$(CC) $(TLDFLAGS) $(CFLAGS) $^ -o $@

%.o : %.c
	$(CC) $(CFLAGS)  -o $@ -c $<

clean:
	rm $(SD)/*.o

mrproper: clean
	rm $(SLIB) $(DLIB)

.PHONY: clean mrproper


