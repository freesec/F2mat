**Introduction**

F2mat C library allows to manipulate matrices defined over the field F2.

The main advantage brought by this library is the optimized encoding of matrices (F2Mat structure).</br>
F2 scalar entries are not represented individually but grouped by blocks of 64 bits (using uint64_t type).</br>
Hence, the matrices are represented by a sequence of uint64_t blocks in a column-major order manner.</br>
Vectors are also available as a specialized type: F2Vec (not really a type but kind of with C preprocessor).</br>

**Install and Compiling**

This library targets Unix-like systems (Linux, Mac OS, etc.) but Windows should be compatible by any means (Cygwin, Mingw, WSL, ...).
The Makefile relies on GNU GCC compiler (you can adjust to another compiler if necessary, clang for example should be easy to move to by changing the CC variable in the makefile).

	git clone --recursive https://gitlab.com/freesec/F2mat F2mat
	cd F2mat
	git submodule foreach make
	make
	# or: make CC=clang # if you want to use clang instead of gcc for compiling
	# it will build static and shared libraries in $PWD
	# the C header is located in src directory
	# now you have all you need to built some code upon this library
	# by the way, compile and run the tests:
	make test
	# or again: make test CC=clang
	./test_F2_mat
	# NOTE: you need to install CUnit to compile and run the tests

**Basic example**

The example below, basically initializes a random matrix over F2 and prints it to the standard output.

	#include "F2_mat.h"
	int main()
	{
		// initialize a 10x12 matrix over F2
		F2Mat* M = f2m_new(10, 12);
		// randomly change its entries
		f2m_set_rand(M);
		// print the matrix
		f2m_print(M);
		// free the matrix instance (important!)
		f2m_del(M);
	}

NOTE: you can also use the F2Vec type the same way as above but replacing the prefix \`f2m' by \`f2v'.

Compile and run the code above :

	# From the parent directory of F2mat
	gcc test.c -IF2mat/src ./F2mat/F2mat.a ./F2mat/nix_cprng/nix_cprng.a
	./a.out

NOTE: it will be cleaner if you edit your own makefile.

**Available Features / Functions and Documentation**

Unfortunately, no independent documentation is available right now, but the library C header is documented (src/F2M_mat.h) and some unit tests in src/test_F2_mat.c can be helpful as they show examples.

Let's mention a few features though:

- Matrix entries access and editing,
- sum of matrices,
- vector dot product,
- matrix-matrix multiplication,
- matrix-vector multiplication,
- transpose matrix,
- Gauss-Jordan elimination and matrix inverse.

**About this project**

This library is a small personal project. I used it as an utility in other projects and I decided to share the code.
The goal of the project is not to become a big library like other exist already but to keep it light and close to my own needs. It doesn't exclude any evolution though.

**How to Contribute**

Feel free to contribute, there is a TODO file if you want to help but any suggestion is welcome of course. Merge requests are welcome too.
Note that the code is under AGPL.

**Credits**

freesec will be my pseudonym here ;-).
You're welcome to contact me by email. My address is freesec2021 at protonmail.com


