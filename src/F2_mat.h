/**
* 	F2mat -- C lib. to manipulate matrices defined over the field F2.
*   Author: freesec2021 at protonmail.com
*   Copyright (C) 2019 freesec (pseudonym)
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published
*   by the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Affero General Public License for more details.
*
*   You should have received a copy of the GNU Affero General Public License
*   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/
#ifndef F2_MAT_H_
#define F2_MAT_H_
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#ifndef _ULL_
typedef uint64_t ULL;
#define _ULL_
#endif

#define F2Vec F2Mat
#define f2v_init(v,k) \
	f2m_init(v,k,1)
#define f2v_new(k) \
	f2m_new(k,1)
#define f2v_free(v) \
	assert((v)->n == 1);\
	f2m_free(v)
#define f2v_del(v) \
	assert((v)->n == 1);\
	f2m_del(v)
#define f2v_zero(v) \
	assert((v)->n == 1);\
	f2m_zero(v)
#define f2v_elt(v,i)\
	assert((v)->n == 1);\
	f2m_get(v,i,0)
#define f2v_set(v,i)\
	assert((v)->n == 1);\
	f2m_set(v,i,0)
#define f2v_eq(v,w)\
	f2m_eq(v,w)
#define f2v_set_rand(v)\
	assert((v)->n == 1);\
	f2m_set_rand(v)
#define f2v_print(v)\
	assert((v)->n == 1);\
	f2m_print(v)
#define f2v_tprint(v)\
	assert((v)->n == 1);\
	f2m_tprint(v)
#define f2v_copy(v,w)\
	assert((w)->k == (v)->k && (w)->n ==1);\
	f2m_copy(v,w)
#define f2v_init_with(v,k,elts)\
	f2m_init_with(v,k,1,elts)A
#define f2v_sum(u,v,w)\
	f2m_sum(u,v,w)
#define f2v_sumr(u,v)\
	f2m_sumr(u,v)
#define f2v_f2m_weight(u)\
	f2m_weight(u)

/** The F2-matrix structure. */
typedef struct F2Mat { // over F2 (binary matrix)
	ushort k; // number of rows
	ushort n; // number of columns
	// buffer blocks of the matrix in column major-order
	// elts = (M(0,0),M(1,0),M(2,0),...,M(n-1,0),M(0,1),M(1,1),M(2,1),..., M(k-1,n-1))
	// The element at row i in the column j is in elts[j] at binary rank i
	// (so it is encoded by the number 2^i)
	ULL * elts;
	ushort blocks_per_col;
} F2Mat;

/**
 * Creates a new k*n matrix over F2.
 *
 * Returns the pointer to it if all went good (NULL otherwise).
 *
 * NOTE: the returned instance should be deleted later, calling f2m_del(F2Mat*).
 */
F2Mat* f2m_new(const ushort k, const ushort n);

/**
 * Initializes a k*n binary matrix.
 *
 * The allocation is done in heap (memory storage).
 * All coefficients are set to zero.
 *
 * NOTE: prefer the use of the pair f2m_new(), f2m_del() which are more easily secure.
 */
void f2m_init(F2Mat *, const ushort k, const ushort n);

/**
 * Initializes M with already existing elements buffer. On the contrary f2m_init() allocates a specific buffer.
 * As a consequence, you don't need to call f2m_free() after use and often you must not because elts could be used elsewhere after.
 * */
void f2m_init_with(F2Mat* M, const ushort k, const ushort n, ULL*elts);

/**
 *  Destroys a matrix instance.
 *
 */
void f2m_del(F2Mat*);

/**
 * Frees the internal memory used for a matrix.
 *
 * If the struct F2Mat instance itself was allocated into the free store/heap, it won't be freed.
 *
 * NOTE: prefer the use of the pair f2m_new(), f2m_del() which are more easily secure.
 */
void f2m_free(F2Mat*);

/**
 * Returns a new F2Mat copy of M.
 * User code is responsible to f2m_free() the copy.
 * Likewise, the M and C matrices must be initialized by the callee.
 */
void f2m_copy(F2Mat *M, F2Mat * C);

/**
 * Prints the matrix.
 */
void f2m_print(const F2Mat *);

/**
 * Prints the matrix f2m_transpose.
 */
void f2m_tprint(const F2Mat*);

/**
 * Writes a matrix in file.
 * TODO: error handling.
 */
void f2m_fprint(const F2Mat*, FILE*);

/**
 * Writes a matrix in file.
 * TODO: error handling.
 */
void f2m_ftprint(const F2Mat*, FILE*);

/**
 *  Changes randomly the values of the F2Mat argument.
 */
void f2m_set_rand(F2Mat *);

/**
 * Returns in bytes the size of the memory space allocated to the matrix elements (`elts' attribute).
 */
size_t f2m_blocks_size(F2Mat *);

/**
 * Copies the j-th column of M in col.
 * Returns 1 if succeeds.
 * Returns 0 if it fails (e.g. if j >= M->n).
 * NOTE: col must have been initialized before call with f2m_init() or f2m_new().
 */
int f2m_col(F2Mat * M, F2Vec * col, ushort j);

/**
 * Returns the j-th column of M.
 * Returns NULL if it fails (e.g. if j >= M->n).
 * NOTE: the returned column-matrix must be freed calling f2m_del() on it.
 */
F2Mat* f2m_colr(F2Mat * M, ushort j);

/**
 * Copies a matrix S sub-column into matrix D.
 * sci is the index of the S column to copy.
 * sli is the start line index to copy the sci-th column of S from.
 * dci is the destination column index to write the snl elements.
 * dli is the destination start line index of D to copy the S elements to.
 * snl is the number of column elements to copy from S to D.
 */
void f2m_copy_subcol(F2Mat* S, F2Mat* D, ushort sli, ushort sci, ushort dli, ushort dci, ushort snl);

/**
 * Copies a matrix S subrect (snl x snc) into matrix D.
 * sci is the index of the first S column to copy.
 * sli is the index of the first S row to copy.
 * dci is the destination column index of D to copy the S elements into.
 * dli is the destination start row index of D to copy the S elements into.
 * snl is the row size of the square to copy from S to D.
 * snc is the column size of the square to copy from S to D.
 */
void f2m_copy_submat(F2Mat* S, F2Mat*D, ushort sli, ushort sci, ushort dli, ushort dci, ushort snl, ushort snc);

/**
 * Copies the column S(:,si) into the column D(:,di).
 */
void f2m_copy_col(F2Mat* S, F2Vec* D, ushort si, ushort di);

/**
 * Permutes randomly the columns of M.
 */
void f2m_shuffle_cols(F2Mat * M);

/**
 * Multiplies the matrix A by the matrix B and returns the result.
 *
 * The result has to be deleted later by calling f2m_del(F2Mat*).
 */
F2Mat* f2m_mul_mm(F2Mat* A, F2Mat * B);


/**
 * Mutiplies the f2m_transpose of vector v by the matrix M into the vector vM (a column-vector).
 *
 * NOTE: a vector is always a column-vector in this library, that's why this function treats about the tranpose.
 *
 * If tvM is different to NULL and well initialized then the result will be stored into it.
 * Anyway, the result is returned by the function (whatever vM is.) except if an error (with a message printed on stderr) occurs then NULL is returned.
 *
 * The result has to be deleted later by calling f2m_del(F2Mat*).
 */
F2Vec* f2m_mul_tvm(F2Vec* v, F2Mat* M, F2Vec* vM);

/**
 * Multiplies the matrix M by the vector v into the matrix Mv (if the pointer is not NULL).
 *
 * Anyway, the result is returned by the function. The return value is NULL if an error occurs.
 *
 * The result has to be deleted later by calling f2m_del(F2Mat*).
 */
F2Vec* f2m_mul_mv(F2Mat* M, F2Vec* v, F2Vec* Mv);

/**
 * Returns 1 if A equals B and 0 otherwise.
 */
unsigned char f2m_eq(F2Mat * A, F2Mat * B);

/**
 * Returns the f2m_weight of a vector or a matrix.
 *
 */
ushort f2m_weight(F2Mat * M);

/**
 * Computes the dot product.
 *
 * Returns -1 it the dimensions of the two vectors don't match.
 * */
short f2v_dot(F2Mat* v1, F2Mat * v2);

/**
 * Computes the f2m_sum of matrices M1 and M2 into M3.
 * M1 and M2 must have the same size.
 * Fails with an error message if dimensions don't agree.
 * NOTE: col must have been initialized before call with f2m_init().
*/
void f2m_sum(F2Mat*M1, F2Mat*M2, F2Mat* M3);

/**
 * Computes the f2m_sum of column-matrices M1 and M2.
 * M1 and M2 must have the same size.
 * Fails with an error message if dimensions don't agree.
 * NOTE: the returned pointer must be freed successively with f2m_free() and free() to avoid memory leak.
*/
F2Mat* f2m_sumr(F2Mat* M1, F2Mat * M2);

/**
 * Sets identity matrix into the first k rows and columns of M.
 *
 * If M is a square matrix of order k, then it'll become the identity matrix.
 *
 * NOTE: `M' must have been initialized before.
 *
 * Returns 1 in case of success, 0 otherwise (if M->k < k or I->n < k).
 */
int f2m_set_identity(F2Mat * M, const ushort k);

/**
 * Returns 1 if M is an identity matrix, 0 otherwise.
 */
short f2m_is_identity(F2Mat *M);

/**
 * Sets the element M(i,j) to 1.
 *
 * Returns 1 if succeeded 0 otherwise (if M is NULL or dimension sizes overpassed).
 */
int f2m_set(F2Mat * M, const ushort i, const ushort j);

/**
 * Sets the element M(i,j) to 0.
 *
 * Returns 1 if succeeded 0 otherwise (if M is NULL or dimension sizes overpassed).
 */
int f2m_clr(F2Mat * M, const ushort i, const ushort j);

/**
 * Sets M(i,j) matrix element to bit's value.
 *
 * Returns 1 if succeeded 0 otherwise (if M is NULL or dimension sizes overpassed).
 */
int f2m_set_to(F2Mat * M, const ushort i, const ushort j, const ushort bit);

/**
 * Gets the matrix entries of M (in F2^(k x n)) from the standard input.
 *
 * Returns -1 if an invalid character is entered.
 * Returns -2 if the number of M rows is overpassed.
 *
 */
int f2m_parse_stdin(F2Mat * M, const ushort k, const ushort n);

/**
 * Returns the element M(i,j) or -1 in case of error.
 *
 */
short f2m_get(const F2Mat * M, const ushort i, const ushort j);

/**
 * Transposes the matrix M into tM.
 *
 * tM must be a different instance of F2Mat in memory (in-situ f2m_transpose is not available).
 *
 * Returns -1 if an error occurs, 0 otherwise.
 */
int f2m_transpose(const F2Mat * M, F2Mat * tM);

/**
 * Transposes in-place the square matrix M.
 *
 * M is supposed to be valid (initialized and filled with bits).
 *
 * Returns 0 if the transpose succeeds.
 * Returns -1 if M is NULL.
 * Returns -2 if M is not square.
 */
int f2m_transpose_inplace(F2Mat * M);

/**
 * Sets M to null matrix.
 */
void f2m_zero(F2Mat *M);

/**
 * Computes the echelon form of M.
 *
 * roffset: the row from which to start (normally 0, but if a partial pivoting has already been done before you can continue it).
 * pivot_cols: receives the column indices of pivots.
 * N: a matrix of same size as M to which operates the same operations as those applied to M (if N is the identity matrix for M a nonsingular square matrix, it should returns the inverse of M).
 * Returns r: the rank of the matrix.
 */
int f2m_echelon(F2Mat* M, int roffset, ushort* pivot_cols, F2Mat* N);

/**
 * Tries to invert the matrix M.
 *
 * Returns the attempt of inversion into M_inv and the rank of M as a return value.
 *
 */
int f2m_inv(F2Mat *M, F2Mat *M_inv);

/**
 * Blanks possible unused bits of the columns last blocks (representing the matrix).
 */
void f2m_clr_rem(F2Mat* M);

#endif /* F2_MAT_H_ */
