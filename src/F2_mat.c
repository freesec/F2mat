/**
* 	F2mat -- C lib. to manipulate matrices defined over the field F2.
*   Author: freesec2021 at protonmail.com
*   Copyright (C) 2019 freesec (pseudonym)
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published
*   by the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Affero General Public License for more details.
*
*   You should have received a copy of the GNU Affero General Public License
*   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "F2_mat.h"
#include "prng.h"
#include  <assert.h>

size_t f2m_blocks_size(F2Mat * M)
{
	return (M->blocks_per_col * M->n) * sizeof(ULL);
}

void f2m_init(F2Mat* M, const ushort k, const ushort n)
{
	M->k = k;
	M->n = n;
	ushort blocks_per_col = k >> 6;
	if (k & 63)
		blocks_per_col++;
	M->elts = malloc(blocks_per_col * n * sizeof(ULL));
	M->blocks_per_col = blocks_per_col;
	memset(M->elts, 0, blocks_per_col * n * sizeof(ULL));
}

void f2m_init_with(F2Mat* M, const ushort k, const ushort n, ULL* elts)
{
	M->k = k;
	M->n = n;
	ushort blocks_per_col = k >> 6;
	if (k & 63)
		blocks_per_col++;
	M->blocks_per_col = blocks_per_col;
	M->elts = elts;
}

F2Mat* f2m_new(const ushort k, const ushort n)
{
	F2Mat* M = malloc(sizeof(F2Mat));
	assert(n > 0 && k > 0);
	M->k = k;
	M->n = n;
	ushort blocks_per_col = k >> 6;
	if (k & 63)
		blocks_per_col++;
	M->elts = malloc(blocks_per_col * n * sizeof(ULL));
	M->blocks_per_col = blocks_per_col;
	memset(M->elts, 0, blocks_per_col * n * sizeof(ULL));
	return M;
}

void f2m_free(F2Mat * M)
{
	M->k = M->n = 0;
	free(M->elts);
}

void f2m_del(F2Mat *M)
{
	f2m_free(M);
	free(M);
}

void f2m_clr_rem(F2Mat* M)
{
	// set remaining bits to 0 in each column
	ushort rem_len = 64 * M->blocks_per_col - M->k; // number of garbage bits
	ULL rem_block_mask = 1llu << rem_len;
	rem_block_mask--;
	rem_block_mask = rem_block_mask << (63-rem_len+1);
	rem_block_mask = ~rem_block_mask;
	ushort i;
	for (i = 0; i < M->n; i++)
	{
		M->elts[M->blocks_per_col * i + M->blocks_per_col - 1]
			&= rem_block_mask;
	}
}

void f2m_set_rand(F2Mat * M)
{
	init_PRNG();
	uint ull_len = M->blocks_per_col * M->n; //nb of ULL blocks of the matrix
	uint i;
	for (i = 0; i < ull_len; i++)
	{
		//because RAND_MAX is encoded on 31 bits
		//we need to set the random 64 bits by block of 31 bits: 0..30, 31..61, 62..63
		M->elts[i] = (((ULL) random()) << 62) | (((ULL) random()) << 31) | random();
	}
	// blank remaining bits in last block of each column
	f2m_clr_rem(M);
}

void f2m_copy(F2Mat *M, F2Mat* C)
{
	for(int i=0;i<M->blocks_per_col*M->n;i++)
		C->elts[i] = M->elts[i];
}

void f2m_print(const F2Mat * M)
{
	f2m_fprint(M,stdout);
}

void f2m_fprint(const F2Mat* M, FILE* f)
{
	ushort i, j;
	for (i = 0; i < M->k; i++)
	{
		for (j = 0; j < M->n; j++)
		{
			fprintf(f, "%d ",f2m_get(M,i,j));
		}
		fprintf(f,"\n");
	}

}

void f2m_ftprint(const F2Mat* M, FILE* f)
{
	ushort i, j; //index lignes et colonnes
	for(j=0;j<M->n;j++)
	{
		for(i=0;i<M->k;i++)
		{
			fprintf(f,"%d ",f2m_get(M,i,j));
		}
		fprintf(f,"\n");
	}
}

void f2m_tprint(const F2Mat* M)
{
	f2m_ftprint(M, stdout);
}

void permute_2_cols(F2Mat * M, ushort j1, ushort j2)
{
	ULL tmp;
	ushort i;
	for (i = 0; i < M->blocks_per_col; i++)
	{
		tmp = M->elts[j1 * M->blocks_per_col + i];
		M->elts[j1 * M->blocks_per_col + i] = M->elts[j2 * M->blocks_per_col
			+ i];
		M->elts[j2 * M->blocks_per_col + i] = tmp;
	}
}

void f2m_shuffle_cols(F2Mat * M)
{
	init_PRNG();
	ushort i = 0;
	ushort rcol_idx;
	while (i < M->n)
	{
		rcol_idx = ((ULL) random() * M->n) / RAND_MAX;
		if (rcol_idx > i)
		{
			//permuter les deux colonnes
			permute_2_cols(M, i, rcol_idx);
		}
		i++;
	}
}

int f2m_col(F2Mat * M, F2Mat * col, ushort j)
{
	if(M->k > col->k)
	{
		perror("f2m_col() error: the output column is too small to receive the matrix column.");
		return -1;
	}
	ushort block_idx;
	if (j < M->n)
	{
		for (block_idx = 0; block_idx < M->blocks_per_col; block_idx++)
		{
			col->elts[block_idx] = M->elts[block_idx + j * M->blocks_per_col];
		}
		return 1;
	}
	perror("f2m_col() error: index j is out of bounds of matrix row dimension.");
	return 0; // j >= M->n
}

F2Mat* f2m_colr(F2Mat * M, ushort j)
{
	ushort block_idx;
	F2Mat* col = malloc(sizeof(F2Mat));
	f2m_init(col, M->k, 1);
	if (j < M->n)
	{
		for (block_idx = 0; block_idx < M->blocks_per_col; block_idx++)
		{
			col->elts[block_idx] = M->elts[block_idx + j * M->blocks_per_col];
		}
		return col;
	}
	if(col != NULL && col->k > 0)
	{
		f2m_free(col);
		free(col);
	}
	return NULL; // j >= M->n
}

void f2m_copy_col(F2Mat* S, F2Mat* D, ushort si, ushort di)
{
	assert(di < D->n);
	assert(si < S->n);
	assert(D->k >= S->k);
	ushort block_idx;
	for (block_idx = 0; block_idx < S->blocks_per_col; block_idx++)
	{
		D->elts[block_idx+di*D->blocks_per_col] = S->elts[block_idx+si*S->blocks_per_col];
	}
}

void f2m_copy_subcol(F2Mat* S, F2Mat* D,
		ushort sli, ushort sci,
		ushort dli, ushort dci,
		ushort snl)
{
	f2m_copy_submat(S,D,sli,sci,dli,dci,snl,1);
}

void f2m_copy_submat(F2Mat* S, F2Mat*D, ushort sli, ushort sci, ushort dli, ushort dci, ushort snl, ushort snc)
{
	assert(sli+snl <= S->k);
	assert(dli+snl <= D->k);
	assert(sci+snc <= S->n);
	assert(dci+snc <= D->n);
	ULL smask; // mask of bits to copy for current source block
	ushort dbi; // current dest block index
	ushort sbi; // current source block index
	ushort sboffset; // bit offset in current source block (from which starts the bits seq. to copy)
	ushort dboffset; // likewise for dest block (but for copy dest)
	ushort n; // number of bits to copy for current iteration
	ushort tmp;
	ULL data; // bits to copy
	while(snl>0)
	{
		sbi = sli>>6;
		sboffset = sli&63;
		dbi = dli>>6;
		dboffset = dli&63;
		//copy the maximum number of bits allowing to remain in current source block (sbi)
		// and don't overflow dest block (dbi)
		n = 64 - sboffset;
		tmp = 64 - dboffset;
		n = n>tmp?tmp:n;
		n = n>snl?snl:n;
		if(n < 64)
			smask = (1llu << n)-1;
		else
			smask = 18446744073709551615llu;
		for(int j=0;j<snc;j++)
		{
			data = S->elts[sbi+(sci+j)*S->blocks_per_col] & (smask << sboffset);
			D->elts[dbi+(dci+j)*D->blocks_per_col] |= (data >> sboffset) << dboffset;
		}
		snl-=n;
		sli += n;
		dli += n;
	}
}

F2Mat* f2m_mul_mm(F2Mat* A, F2Mat * B)
{

	ushort j;
	ushort block_idx, aux_block_idx;
	ushort A_col_idx =0;
	ULL elt_mask;
	F2Mat * C;

	if (A->n != B->k) //matrix product not defined
		return NULL;

	C = malloc(sizeof(F2Mat));
	f2m_init(C, A->k, B->n);

	elt_mask = 1llu << 63;

	for (j = 0; j < B->n; j++)
	{
		block_idx = 0;
		A_col_idx = 63;
		while (block_idx < B->blocks_per_col)
		{
			if (elt_mask & B->elts[j * B->blocks_per_col + block_idx])
			{
				for (aux_block_idx = 0; aux_block_idx < C->blocks_per_col; aux_block_idx++)
				{
					C->elts[j * C->blocks_per_col + aux_block_idx]
						^= A->elts[A_col_idx * A->blocks_per_col
						+ aux_block_idx];
				}
			}
			elt_mask >>= 1;
			A_col_idx--;
			if (!elt_mask)
			{
				block_idx++;
				elt_mask = 1llu << 63;
				A_col_idx = block_idx*64+63;
			}

		}
	}

	return C;
}

F2Mat* f2m_mul_tvm(F2Vec* v, F2Mat*M, F2Vec* vM)
{
	if (v->k != M->k)
	{
		perror("f2m_mul_tvm error: the vector length must equal the\
				number of matrix rows.");
		return NULL;
	}
	if(v->n != 1)
	{
		perror("f2m_mul_tvm() error: v is not a vector (attr. n != 1).");
		return NULL;
	}
	if(vM == NULL) vM = malloc(sizeof(F2Mat));
	ushort j; //col G idx
	F2Mat col;
	f2m_init(&col, M->k, 1);
	f2m_init(vM, M->n, 1);
	ushort block_idx; // for cword access
	ushort block_offset;
	ULL block;
	for (j = 0; j < M->n; j++)
	{
		f2m_col(M, &col, j);
		block_idx = j >> 6;
		block_offset = (j & 63);
		// inner product of col and iword, giving j-th coeff of cword
		block = ((ULL) f2v_dot(v, &col)) << block_offset;
		vM->elts[block_idx] |= block;
	}
	f2m_free(&col);
	return vM;
}

F2Mat* f2m_mul_mv(F2Mat* M, F2Vec* v, F2Vec* Mv)
{
	if (v->k != M->n)
	{
		perror("f2m_mul_mv error: the vector length must equal the\
				number of matrix rows.");
		return NULL;
	}
	if(v->n != 1)
	{
		perror("f2m_mul_mv() error: v is not a vector (attr. n != 1).");
		return NULL;
	}
	return f2m_mul_mm(M, v);
}

unsigned char f2m_eq(F2Mat * A, F2Mat * B)
{
	int j;
	int block_idx;

	if (A->k != B->k || A->n != B->n)
		return 0;

	for (j = 0; j < B->n; j++)
	{
		for (block_idx = 0; block_idx < B->blocks_per_col; block_idx++)
		{
			if (A->elts[j * A->blocks_per_col + block_idx] ^ B->elts[j
					* B->blocks_per_col + block_idx])
				return 0;
		}
	}
	return 1;
}

ushort f2m_weight(F2Mat * M)
{
	ushort w = 0;
	ushort block_idx;
	ULL tmp_block;
	for(int j = 0; j < M->n; j++)
		for (block_idx = 0; block_idx < M->blocks_per_col; block_idx++)
		{
			tmp_block = M->elts[block_idx+j*M->blocks_per_col];
			while (tmp_block)
			{
				tmp_block &= (tmp_block - 1);
				w++;
			}
		}
	return w;
}

short f2v_dot(F2Vec* v1, F2Vec * v2)
{
	if (v1->blocks_per_col != v2->blocks_per_col || v1->k != v2->k || v1->n
			!= v2->n || v1->n != 1)
	{
		perror("f2v_dot() error: vector dimensions must agree.");
		return -1;
	}
	ushort block_idx;
	F2Mat v3;
	ushort prod;
	//initialiser v3
	f2m_init(&v3, v1->k, v1->n);

	for (block_idx = 0; block_idx < v1->blocks_per_col; block_idx++)
	{
		v3.elts[block_idx] = v1->elts[block_idx] & v2->elts[block_idx];
	}

	prod = f2m_weight(&v3) & 1;
	f2m_free(&v3);
	return prod;
}

void f2m_sum(F2Mat* M1, F2Mat * M2, F2Mat * M3)
{
	if (M1->blocks_per_col != M2->blocks_per_col || M1->k != M2->k || M1->n
			!= M2->n)
	{
		perror("f2v_sum() error: vector dimensions must agree.");
	}
	ushort block_idx;
	ushort bpc = M1->blocks_per_col;
	for(int j=0;j < M1->n; j++)
		for (block_idx = 0; block_idx < bpc; block_idx++)
			M3->elts[block_idx+j*bpc] = M1->elts[block_idx+j*bpc] ^ M2->elts[block_idx+j*bpc];
}

F2Mat* f2m_sumr(F2Mat* M1, F2Mat * M2)
{
	F2Mat * M3;
	M3 = malloc(sizeof(F2Mat));
	f2m_init(M3, M1->k, M1->n);
	f2m_sum(M1,M2,M3);
	return M3;
}

int f2m_set_identity(F2Mat * M, const ushort k)
{
	ushort i;
	ushort block_idx, block_offset;
	if(M->k < k || M->n < k) return 0;
	for (i = 0; i < k; i++)
	{
		block_idx = i >> 6;
		block_offset = i & 63;
		M->elts[i * M->blocks_per_col + block_idx] = 1llu
			<< block_offset;
	}
	return 1;
}

short f2m_is_identity(F2Mat * M)
{
	ushort i;
	ushort block_idx, block_offset;
	if(M->k != M->n) return 0;
	for (i = 0; i < M->k; i++)
	{
		block_idx = i >> 6;
		block_offset = i & 63;
		if(M->elts[i * M->blocks_per_col + block_idx] != 1llu << block_offset) return 0;
	}
	return 1;
}

int f2m_set(F2Mat * M, const ushort i, const ushort j)
{
	ushort block_idx, block_offset;
	if (M != NULL && i < M->k && j < M->n)
	{
		block_idx = i >> 6;
		block_offset = i & 63;
		M->elts[j * M->blocks_per_col + block_idx] |= 1llu << block_offset;
		return 1;
	}
	return 0;
}

int f2m_clr(F2Mat * M, const ushort i, const ushort j)
{
	ushort block_idx, block_offset;
	if (M != NULL && i < M->k && j < M->n)
	{
		block_idx = i >> 6;
		block_offset = i & 63;
		M->elts[j * M->blocks_per_col + block_idx] &= ~(1llu << block_offset);
		return 1;
	}
	return 0;
}

int f2m_set_to(F2Mat * M, const ushort i, const ushort j, const ushort bit)
{
	if(bit)
		return f2m_set(M,i,j);
	else
		return f2m_clr(M,i,j);
}

short f2m_get(const F2Mat * M, const ushort i, const ushort j)
{
	ushort block_idx, block_offset;
	if (M != NULL && i < M->k && j < M->n)
	{
		block_idx = i >> 6;
		block_offset = i & 63;
		return (M->elts[j * M->blocks_per_col + block_idx] & (1llu << block_offset))>0;
	}
	return -1;
}

int f2m_parse_stdin(F2Mat * M, const ushort k, const ushort n)
{
	char elt;
	f2m_init(M, k, n);
	ushort i = 0, j = 0;
	while (i < k)
	{
		scanf("%c", &elt);
		if (elt == '1')
		{
			f2m_set(M, i, j);
			j++;
		} else if (elt == '0')
			j++;
		else if (j == n && elt == '\n')
		{
			i++;
			j = 0;
		} else if (elt == '\n')
		{
			return -2;
		}

		else if (elt != ' ')
		{
			return -1;
		}
	}
	return 0;
}

int f2m_transpose(const F2Mat * M, F2Mat * tM)
{
	if (M == NULL || tM == NULL)
		return -1;
	short elt;
	ushort i, j;
	//TODO: don't initialize twice (check in f2m_init directly)
	if(tM->k != M->n || tM->n != M->k)
		f2m_init(tM, M->n, M->k);
	for (i = 0; i < M->k; i++)
	{
		for (j = 0; j < M->n; j++)
		{
			elt = f2m_get(M, i, j);
			if (elt < 0)
				return -1;
			else if (elt)
				f2m_set(tM, j, i);
		}
	}
	return 0;
}

int f2m_transpose_inplace(F2Mat * M)
{
	short tmp;
	ushort i, j;
	if (M == NULL)
		return -1;
	if(M->k != M->n)
			return -2;
	for (i = 0; i < M->k; i++)
	{
		for (j = 0; j < i; j++)
		{
			tmp = f2m_get(M, i, j);
			f2m_set_to(M, i, j, f2m_get(M, j, i));
			f2m_set_to(M, j, i, tmp);
		}
	}
	return 0;
}

void f2m_zero(F2Mat *M)
{
	bzero(M->elts, sizeof(ULL)*M->blocks_per_col*M->n);
}

int f2m_inv(F2Mat *M, F2Mat *M_inv)
{
	//TODO: don't do anything if M is not square
	F2Mat M_ech;
	f2m_init(&M_ech, M->k, M->n);
	f2m_copy(M, &M_ech);
	f2m_set_identity(M_inv, M->k);
	int ret = f2m_echelon(&M_ech, 0, NULL, M_inv);
	f2m_free(&M_ech);
	return ret == M->n;
}

int f2m_echelon(F2Mat* M, int loffset, ushort* pivot_cols, F2Mat* N)
{
	int i,j;
	int pi; //pivot column index to become the i-th column echelon matrix
	F2Mat pcol /* pivot column */, pcol2;
	F2Mat ccol; //current column
	F2Mat scol; //f2m_sum column
	assert(N==NULL || (M->n == N->n && M->k == N->k));
	f2m_init(&pcol, M->k, 1);
	if(N != NULL)
		f2m_init(&pcol2, M->k, 1);
	f2m_init(&ccol, M->k, 1);
	f2m_init(&scol, M->k, 1);
	int min_nk = M->n<M->k?M->n:M->k;
	for(i=0;i<min_nk;i++)
	{
		pi = i;
		assert(i+loffset < M->k && pi < M->n); //TODO: replace by an error handling
		while(!f2m_get(M,i+loffset,pi) && pi < M->n) //f2m_get returns -1 if coords are outside the matrix
			pi++;
		if(pi >= M->n)
			return i; // M's not invertible
		if(i != pi)
		{
			permute_2_cols(M,pi,i);
			if(N!=NULL)
				permute_2_cols(N,pi,i);
		}
		if(pivot_cols != NULL)
			pivot_cols[i] = pi;
		//		printf("f2m_echelon() %d-th pivot column found, currently is %d-th column\n",i,pi);
		pi = i;
		f2m_col(M, &pcol, pi);
		if(N != NULL)
			f2m_col(N, &pcol2, pi);
		for(j=0;j<M->n;j++)
		{
			if(pi == j) continue;
			if(f2m_get(M,pi+loffset,j))
			{
				f2m_col(M,&ccol, j);
				f2m_sum(&ccol,&pcol,&scol);
				f2m_copy_col(&scol, M, 0, j);
				if(N != NULL)
				{
					f2m_col(N,&ccol, j);
					f2m_sum(&ccol,&pcol2,&scol);
					f2m_copy_col(&scol, N, 0, j);
				}
			}
		}
	}
	f2m_free(&pcol);
	f2m_free(&scol);
	f2m_free(&ccol);
	if(N != NULL)
		f2m_free(&pcol2);
	return i;
}
