/**
* 	F2mat -- C lib. to manipulate matrices defined over the field F2.
*   Author: freesec at linuxmail.org
*   Copyright (C) 2019 freesec (pseudonym)
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published
*   by the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Affero General Public License for more details.
*
*   You should have received a copy of the GNU Affero General Public License
*   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/
#include "F2_mat.h"
#include <CUnit/Basic.h>
#include <assert.h>
#include "prng.h"


int init_F2_mat_suite(){
	init_PRNG();
	return 0;
}

int clean_F2_mat_suite(){
	return 0;
}

void gen_rand_mat(ushort n_ones, ushort* one_ids, F2Mat* M)
{
	ushort already_got ;
	int i,j;
	already_got = i = 0;
	ushort col_vec = M->n == 1;
	ushort size = M->k;
	//pick random coords
	while(i < n_ones)
	{
		one_ids[i*2] = ((ushort)random())*size/(ushort)RAND_MAX; //row id
		one_ids[i*2+1] = col_vec?0:(((ushort)random())*size/(ushort)RAND_MAX); //col id
		j = i>0?i-1:i;
		while(j>=0 && i>0)
		{
			if((already_got = (one_ids[j*2] == one_ids[i*2] && one_ids[j*2+1] == one_ids[i*2+1])))
				break;
			j--;
		}
		if(! already_got)
			i++;
	}
	// set matrix ones to 1
	f2m_zero(M);
	ushort last_f2m_weight = 0;
	ushort new_f2m_weight = 0;
	for(int i=0;i<n_ones;i++)
	{
		f2m_set(M, one_ids[i*2], one_ids[i*2+1]);
		last_f2m_weight = new_f2m_weight;
		new_f2m_weight = f2m_weight(M);
		if(new_f2m_weight != last_f2m_weight+1)
		{
			printf("i=%u j=%u\n", one_ids[i*2], one_ids[i*2+1]);
			for(int j=0;j<n_ones;j++)
				printf("i=%u j=%u, ", one_ids[j*2], one_ids[j*2+1]);
			printf("\n");
		}
	}
}

void test_f2m_weight()
{
	ushort max_size = 100;
	ushort size, n_ones;
	ushort *one_ids;
//	ushort already_got ;
	F2Mat M;
	printf("\n");
	// test f2m_weight on 2D matrix in the first ite
	// test f2m_weight on column-vec in the 2nd ite
	for(int col_vec=0;col_vec<2;col_vec++)
	{
		size = random()*max_size/RAND_MAX;
		n_ones = size/(col_vec?1:2)*random()/RAND_MAX;
		one_ids = malloc(n_ones*2*sizeof(ushort));
		f2m_init(&M, size, col_vec?1:size);
		gen_rand_mat(n_ones, one_ids, &M);
//		f2m_print(&M);
		printf("f2m_weight(M)=%u n_ones=%u\n", f2m_weight(&M), n_ones);
		CU_ASSERT_TRUE(f2m_weight(&M) == n_ones);
		f2m_free(&M);
		free(one_ids);
	}
}

void test_f2v_dot()
{
	// we assume f2m_weight() works (ensured by another unit test)
	// we assume f2m_sumr() works also (ensured in another ut)
	F2Vec v1, v2, *v3, *v1_plus_v3, *v2_plus_v3;
	ushort d;
	int max_size = 100;
	int k = random()*max_size/RAND_MAX;
	f2v_init(&v1,k);
	f2v_init(&v2,k);
	f2v_set_rand(&v1);
	f2v_set_rand(&v2);
	d = f2v_dot(&v1,&v1);
	CU_ASSERT_TRUE(d == (f2m_weight(&v1) & 1u));
	v3 = f2v_sumr(&v1, &v2);
	v2_plus_v3 = f2v_sumr(&v2, v3);
	v1_plus_v3 = f2v_sumr(&v1, v3);
	CU_ASSERT_EQUAL(f2v_dot(&v1,&v2), f2v_dot(v2_plus_v3,&v2));
	CU_ASSERT_EQUAL(f2v_dot(&v1,&v2), f2v_dot(&v1, v1_plus_v3));
	f2v_free(&v1);
	f2v_free(&v2);
	f2v_free(v3);
	f2v_free(v2_plus_v3);
	f2v_free(v1_plus_v3);
	free(v2_plus_v3);
	free(v1_plus_v3);
}

void test_sum()
{
	//assuming f2m_eq() works
	F2Mat M1, M2, *M3, *M1_plus_M3, *M2_plus_M3;
	int max_size = 10;
	int k = random()*max_size/RAND_MAX;
	int n = random()*max_size/RAND_MAX;
	f2m_init(&M1,k,n);
	f2m_init(&M2,k,n);
	f2m_set_rand(&M1);
	f2m_set_rand(&M2);
	M3 = f2m_sumr(&M1, &M2);
	M2_plus_M3 = f2m_sumr(&M2, M3);
	M1_plus_M3 = f2m_sumr(&M1, M3);
//	printf("M1=\n");
//	f2m_print(&M1);
//	printf("M2=\n");
//	f2m_print(&M2);
//	printf("M3=\n");
//	f2m_print(M3);
	CU_ASSERT_TRUE(f2m_eq(M2_plus_M3, &M1));
	CU_ASSERT_TRUE(f2m_eq(M1_plus_M3, &M2));
	f2m_free(&M1);
	f2m_free(&M2);
	f2m_free(M3);
	f2m_free(M2_plus_M3);
	f2m_free(M1_plus_M3);
	free(M2_plus_M3);
	free(M1_plus_M3);
}

void test_f2m_mul_tvm()
{
	F2Mat M, tM;
	F2Vec v;
	F2Vec Mrow;
	F2Vec csum;
	F2Vec *mul_res;
	int k = 10;
	int n = 12;
	int row_id;
	ushort n_ones;
	ushort * one_ids;
	f2m_init(&M, k, n);
	f2m_set_rand(&M);
	f2v_init(&v, k);
	f2v_init(&Mrow, n);
	f2v_init(&csum, n);
	n_ones = k*random()/RAND_MAX;
	one_ids = malloc(n_ones*2*sizeof(ushort));
	gen_rand_mat(n_ones, one_ids, &v);
	f2m_transpose(&M, &tM);
	for(int i=0;i<n_ones; i++)
	{
		row_id = one_ids[i*2];
		f2m_col(&tM, &Mrow, row_id);
		f2v_sum(&Mrow, &csum, &csum);
	}
	mul_res = f2m_mul_tvm(&v, &M, NULL);
//	f2v_tprint(mul_res);
//	f2v_tprint(&csum);
	CU_ASSERT_TRUE(f2v_eq(&csum, mul_res));
}

void test_f2m_mul_mv()
{
	F2Mat M;
	F2Vec v;
	F2Mat tM;
	int k = 10;
	int n = 12;
	f2m_init(&M, k, n);
	f2m_set_rand(&M);
	f2v_init(&v, n);
	f2v_set_rand(&v);
	f2m_init(&tM, n, k);
	f2m_transpose(&M, &tM);
	F2Vec *ref = f2m_mul_tvm(&v,&tM, NULL);
	F2Vec *test = f2m_mul_mv(&M, &v, NULL);
//	f2m_tprint(ref);
//	f2m_tprint(test);
	CU_ASSERT_TRUE(f2m_eq(ref,test));
	f2m_free(ref);
	f2m_free(test);
	free(ref);
	free(test);
}

void test_new_del()
{
	F2Mat* A = f2m_new(100,128);
	F2Mat* B = f2m_new(100,128);
	f2m_set_rand(A);
	f2m_set_rand(B);
	F2Mat* C, *T;
	C = f2m_sumr(A,B);
	CU_ASSERT_TRUE(C->n == B->n && C->k == B->k);
	T = f2m_sumr(B,C);
	CU_ASSERT_TRUE(f2m_eq(T,A));
	f2m_del(T);
	T = f2m_sumr(A,B);
	CU_ASSERT_TRUE(f2m_eq(T,C));
	f2m_del(T);
	f2m_del(B);
	f2m_del(A);
	f2m_del(C);
	// really speedy test for vectors
	F2Vec* u = f2v_new(100);
	F2Vec* v = f2v_new(100);
	F2Vec* w, *t;
	f2v_set_rand(u);
	f2v_set_rand(v);
	w = f2v_sumr(u,v);
	t = f2v_sumr(v,w);
	CU_ASSERT_TRUE(f2v_eq(t,u));
	f2v_del(t);
	t = f2v_sumr(w,u);
	CU_ASSERT_TRUE(f2v_eq(t,v));
	f2v_del(t);
	f2v_del(u);
	f2v_del(v);
}

void test_echelon()
{
	ushort size = 100;
	F2Mat* A = f2m_new(size,size);
	F2Mat* Ir;
	f2m_set_rand(A);
	int r = f2m_echelon(A, 0, NULL, NULL);
//	printf("\ntest ech:\n");
//	f2m_print(A);
//	printf("rank = %d\n", r);
	CU_ASSERT_TRUE(r < size || f2m_is_identity(A));
	if(r < size)
	{
		Ir = f2m_new(r,r);
		f2m_copy_submat(A, Ir, 0, 0, 0, 0, r, r);
		CU_ASSERT_TRUE(f2m_is_identity(Ir));
		f2m_del(Ir);
	}
	f2m_del(A);
}

void test_transpose_inplace()
{
	ushort nrows, ncols;
	nrows = 10;
	ncols = 10;
	F2Mat* M = f2m_new(nrows, ncols);
	F2Mat* M_copy = f2m_new(nrows, ncols);
	f2m_set_rand(M);
	f2m_copy(M, M_copy);
	f2m_transpose_inplace(M);
	for(int i=0; i < nrows; i++)
		for(int j=0; j < ncols; j++)
		{
			CU_ASSERT_EQUAL(f2m_get(M, i, j), f2m_get(M_copy, j, i));
		}
	CU_ASSERT_EQUAL(M->n, M->k)
	CU_ASSERT_EQUAL(M_copy->k, M->n)
	f2m_free(M);
	f2m_free(M_copy);
}

void test_transpose()
{
	ushort nrows, ncols;
	nrows = 10;
	ncols = 15;
	F2Mat* M = f2m_new(nrows, ncols);
	F2Mat* tM = f2m_new(ncols, nrows);
	f2m_set_rand(M);
	f2m_transpose(M, tM);
	for(int i=0; i < nrows; i++)
		for(int j=0; j < ncols; j++)
		{
			CU_ASSERT_EQUAL(f2m_get(M, i, j), f2m_get(tM, j, i));
		}
	CU_ASSERT_EQUAL(M->n, tM->k)
	CU_ASSERT_EQUAL(tM->k, M->n)
	f2m_free(M);
	f2m_free(tM);
}

void test_inv()
{
	ushort nrows, ncols;
	nrows = 12;
	ncols = 12;
	F2Mat* M = f2m_new(nrows, ncols);
	F2Mat* M_inv = f2m_new(nrows, ncols);
	F2Mat* id = f2m_new(nrows, ncols);
	F2Mat* M_M_inv = NULL;
	f2m_set_identity(id, nrows);
	f2m_set_rand(M);
	int M_is_inv = f2m_inv(M, M_inv);
	M_M_inv = f2m_mul_mm(M, M_inv);
	CU_ASSERT_TRUE(f2m_is_identity(M_M_inv) || !M_is_inv);
	f2m_free(M_M_inv);
	f2m_free(M);
	f2m_free(M_inv);
}

/* The main() function for setting up and running the tests.
 *  Returns a CUE_SUCCESS on successful running, another
 *  CUnit error code on failure.
 **/
int main()
{
	/* initialize the CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry())
		return CU_get_error();

	/* add a suite to the registry */
	CU_pSuite F2mat_suite = CU_add_suite("F2mat test suite", init_F2_mat_suite, clean_F2_mat_suite);
	if(!F2mat_suite) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	CU_add_test(F2mat_suite, "test_f2m_weight()", test_f2m_weight);
	CU_add_test(F2mat_suite, "test_dot()", test_f2v_dot);
	CU_add_test(F2mat_suite, "test_sum()", test_sum);
	CU_add_test(F2mat_suite, "test_f2m_mul_tvm()", test_f2m_mul_tvm);
	CU_add_test(F2mat_suite, "test_f2m_mul_mv()", test_f2m_mul_mv);
	CU_add_test(F2mat_suite, "test_new_del()", test_new_del);
	CU_add_test(F2mat_suite, "test_echelon()", test_echelon);
	CU_add_test(F2mat_suite, "test_transpose_inplace()", test_transpose_inplace);
	CU_add_test(F2mat_suite, "test_transpose()", test_transpose);
	CU_add_test(F2mat_suite, "test_inv()", test_inv);
	/* Run all tests using the CUnit Basic interface */
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return CU_get_error();
}
